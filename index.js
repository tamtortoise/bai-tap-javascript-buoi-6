// Bài 1 Số nguyên dương

function ketQuaSoNguyen() {
    var soNguyenduongnhonhat = 0;
    var sum = 0;
    for ( var i = 0; sum < 10000; i++) {
        sum += i;
        soNguyenduongnhonhat = i;
    }
    document.getElementById("result01").innerHTML = ` Số nguyên dương nhỏ nhất là ${soNguyenduongnhonhat}`;
}

// Bài 2 Tính tổng

// Dùng hàm Math.pow 

function ketQua() {
    var x = document.getElementById("txt-X").value*1;
    var y = document.getElementById("txt-Y").value*1;
    var sum = 0;
    for (i = 1; i <= y; i++) {
        sum = sum + Math.pow(x,i);
        console.log('sum: ', sum);
    }
    document.getElementById("result02").innerHTML = ` Tổng là ${sum}`;
}

// Bài 3

// Giai thừa: Factorial

function giaiThua() {
    var number = document.getElementById("txt-n").value*1;
    var factorial = 1;
    for ( let i = 1; i <= number; i++) {
        factorial *= i;
    }
    document.getElementById("result03").innerHTML=` Giai thừa là ${factorial}`
}

// Bài 4

function taoThe() {

    var divs = document.getElementsByTagName("div");
    for (var i = 0; i < divs.length; i++){
        // Vị trí chẵn => màu xanh
        if ((i + 1) % 2 == 0){
            divs[i].style.background = "yellow";
        }
        else { // Vị trí lẽ => màu xanh lá
            divs[i].style.background = "green";
        }
    }
}


